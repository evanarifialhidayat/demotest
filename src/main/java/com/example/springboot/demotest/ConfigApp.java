package com.example.springboot.demotest;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.example.springboot.controller"})
public class ConfigApp {
//    configurasi apps
}
