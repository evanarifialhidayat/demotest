package com.example.springboot.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    private final String UP = "UP";
    private final String DOWN = "DOWN";
    private final String INCORECT = "INCORECT";


    @GetMapping("/tes")
    public String tesurl(@RequestParam String urls){
        String return_mesegae = "";
        try {
            URL url = new URL(urls);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();
            int resp = httpURLConnection.getResponseCode() / 100;
            if( resp != 2){
                return_mesegae = DOWN;
            }else{
                return_mesegae = UP;
            }
        } catch (MalformedURLException e) {
            return_mesegae = INCORECT;
        }catch(IOException ee){
            return_mesegae = DOWN;
        }
        
        return return_mesegae;
    }
}
